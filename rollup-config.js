
// In a console, run:
// rollup -c rollup-config.js


export default {
	input: 'src/soclial.js',
	output: {
		file: 'dist/soclial.js',
// 		format: 'cjs',
		format: 'umd',
		sourcemap: true,
	},
	plugins: [
	// 		require('rollup-plugin-string')({ extensions: ['.glsl'] }),
// 			require('rollup-plugin-buble')(),
	// 		require('rollup-plugin-commonjs')(),
	// 		require('rollup-plugin-node-resolve')()
	],
	external: [
		'fs',
		'inquirer',
		'oauth',
// 		'chalk256',
		'mastodon-api',
		'striptags',
		'he',
		'twitter',
		'vorpal',
		'configstore',
		'flatten-obj',
		'tinyqueue',
		'url-regex'
	]
};

