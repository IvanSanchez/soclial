
`soCLIal` is a NodeJS social networking client with a command-line interface.

It's social, and it's CLI. So, soCLIal.

As of now, soCLIal is feature-incomplete. The current list of features is:
- Sign into a `mastodon.social` account, and display your home timeline.
- Sign into a twitter account, and display your home timeline.
- Display statuses, RTs/boosts, and quoted statuses in a compact and legible way
- Post new statuses (text only)
- Reply to statuses
- Echo (retweet/boost/reshare) statuses

The interface is meant to be as close as TTYtter/oysttyer as possible. Interactive
shell, streaming of messages by default, different colours for different kinds
of messages.

![](screenshot.png)

### Quickstart

There are no releases yet, so

* `git clone` this repo
* Run either `npm install` or `yarn` to fetch dependencies
* Run either `npm run-script build` or `yarn build`
* Run `nodejs dist/soclial.js`

Once soCLIal is up and running:
* Type `help` for a list of commands
* Type `accounts add` to add an account using an interactive prompt



### TODO:

- Cover edge cases of replying and echoing statuses, improve error handling
- Display threads
- Allow posting and echoing to a subset of the accounts
- Implement channels (replies/notifications/direct messages/search terms/user lists)
- Allow listening/unlistening to channels
- Offer platforms (and plugins?) a better API surface of the currently running
    soclial instance (instead of just a reference to Vorpal and a handler)





