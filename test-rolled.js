import platforms_clock from 'platforms/clock';
import Vorpal from 'vorpal';

const vorpal = Vorpal();

vorpal
	.command('foo', 'Outputs "bar".')
	.action(function(args, callback) {
		this.log('bar');
		callback();
	});

vorpal
	.delimiter('soCLIal> ')
	.show();
