import chalkPipe from "chalk-pipe";
import supportsColor from "supports-color";

// Given a timestamp in millisecs, format it into a string.
const startTime = new Date();
const timezoneOffset = startTime.getTimezoneOffset() * 60000; // In millisecs
function formattedTimestamp(timestamp = Date.now()) {
	// https://stackoverflow.com/questions/10645994/node-js-how-to-format-a-date-string-in-utc
	return new Date(timestamp - timezoneOffset)
		.toISOString()
		.replace(/T/, " ") // replace T with a space
		.replace(/\..+/, ""); // delete the dot and everything after
}

// Given an instance of ConfigStore, return logger functions
export default function getLoggers(conf, vorpal) {
	function getLoggerForColours(colourSection, prefix) {
		return function() {
			const colourFn = chalkPipe(conf.get("global.colours." + colourSection));
			const colourFnAlt = chalkPipe(
				conf.get("global.colours." + colourSection + "Alt")
			);

			let first =
				"[" + colourFn(formattedTimestamp()) + "] " + colourFnAlt(prefix);

			const colourArgs = [first].concat(
				Array.prototype.map.call(arguments, (a, i) =>
					(i % 2 ? colourFnAlt : colourFn)(
						typeof a === "string" || a.toString !== "[object Object]"
							? a
							: JSON.stringify(a)
					)
				)
			);

			vorpal.log.apply(vorpal, colourArgs);
		};
	}

	const masterLog = getLoggerForColours("system", ">>>>");
	const masterError = getLoggerForColours("error", ">>>>");

	masterLog("User configuration file read.");
	//     masterError('This is how an error looks like.', 1, 2,3,4);

	{
		const colorLevel = supportsColor.supportsColor().level;
		switch (colorLevel) {
			case 0:
				masterLog("Terminal does not support colour");
				break;
			case 1:
				masterLog("Terminal supports basic colour");
				break;
			case 2:
				masterLog("Terminal supports 256 colours");
				break;
			case 3:
				masterLog("Terminal supports 16m colours (truecolour)");
				break;
			default:
				masterError("Unknown color support level: ", colorLevel);
		}
	}

	return {
		formattedTimestamp,
		getLoggerForColours,
		masterLog,
		masterError,
	};
}
