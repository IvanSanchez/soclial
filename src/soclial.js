// import Clock from './platforms/clock';

import * as platforms from "./platforms/index";
import * as circularBuffer from "./circularBuffer";
import stringify from "./stringify";

import chalkPipe from "chalk-pipe";
import Vorpal from "vorpal";
import inquirer from "inquirer";
import Configstore from "configstore";
import CommandLineArgs from "command-line-args";
import CommandLineUsage from "command-line-usage";
import Flatten from "flatten-obj";
import TinyQueue from "tinyqueue";

import getLoggers from "./loggers";
const flatten = Flatten();
const vorpal = Vorpal();

import deshortifyFilter from "./filters/deshortify";

vorpal.history("soclial");
vorpal.log(">>> Initializing soCLIal...");

const cmdOptionsDef = [
	{
		name: "configname",
		alias: "c",
		type: String,
		description:
			'The name of the config file to be used (sans the .json extension). Defaults to "soclial"',
		defaultValue: "soclial",
	},
	{
		name: "configpath",
		alias: "p",
		type: String,
		description:
			'Path to the config file to be used. Defaults to "~/.config/configstore/"',
	},
	{
		name: "help",
		alias: "h",
		type: Boolean,
		description: "Display this help.",
	},
];

const cmdOptions = CommandLineArgs(cmdOptionsDef);

if (cmdOptions.help) {
	const cmdLineUsageConfig = [
		{
			header: "soclial",
			content:
				"A command-line interface to social networks (twitter, mastodon).",
		},
		{
			header: "Command-line options",
			optionList: cmdOptionsDef,
		},
		{
			header: "Runtime help",
			content:
				'Once in the interactive soclial prompt, type "help" and press enter to list available commands.',
		},
	];

	console.log(CommandLineUsage(cmdLineUsageConfig));
	process.exit(0);
}

const configStoreOptions = {};
if (cmdOptions.configpath) {
	configStoreOptions.configPath = cmdOptions.configpath;
}

// create a Configstore instance with an unique ID e.g.
// Package name and optionally some default values
const conf = new Configstore(
	cmdOptions.configname,
	{
		// Default options, written to config file when created
		global: {
			colours: {
				// These are chalk-pipe (https://github.com/LitoMore/chalk-pipe)
				// style definitions
				timestamp: "limegreen",
				badge: "pink",
				verbose: "dim",
				verboseAlt: "dim.italic",
				system: "yellow",
				systemAlt: "orange",
				error: "red",
				errorAlt: "lightred",
				prompt: "white.bold",
				promptAlt: "white.bold.italic.underline",
				updates: "white",
				media: "cyan",
				usernames: "underline",
			},
		},
		accounts: {},
	},
	configStoreOptions
);

const { formattedTimestamp, getLoggerForColours } = getLoggers(conf, vorpal);
const masterVerbose = getLoggerForColours("verbose", ">>>>");
const masterLog = getLoggerForColours("system", ">>>>");
const masterError = getLoggerForColours("error", ">>>>");
const masterPrompt = getLoggerForColours("prompt", ">>>>");

// Define a tiniqueue for the items. This will allow fetching the Promise for
// the oldest update. Also the accounts can block the queue with synthetic
// promises that resolve to a falsy value.
// The queue will store entries in a { timestamp: 1234, itemPromise: Promise(...) } format.
function compareQueueItems(a, b) {
	return a.timestamp - b.timestamp;
}
let itemQueue = new TinyQueue([], compareQueueItems);

const deshortifyMaster = {
	verbose: getLoggerForColours("verbose", "url>"),
	log: getLoggerForColours("system", "url>"),
	error: getLoggerForColours("error", "url>"),
	prompt: getLoggerForColours("prompt", "url>"),
};

// This handles new item Promises from the accounts. Put them in the queue,
// wait for the oldest to resolve.
function handle(timestamp, itemPromise) {
	// Pass the item through all known filters
	itemPromise = itemPromise.then(
		(item) => item && deshortifyFilter(deshortifyMaster, item)
	);

	itemQueue.push({ timestamp: timestamp, itemPromise: itemPromise });

	// 	masterVerbose('Pushed into queue:', timestamp, Date.now() - timestamp, "now " + itemQueue.length + ' items pending.');

	// Is this now the only item in the queue? Let's trigger a queue fetch in a short
	// while.
	if (itemQueue.length === 1) {
		// // 		vorpal.log(chalk.purple('>>> Nothing in the queue, let\'s wait a bit to pop the oldest item.'));
		popOldest();
	}
}

// Pops the oldest in the queue, to stringify and display it.
// If there are no items left, do nothing. A new handle() is supposed to trigger
// this again.
function popOldest() {
	if (itemQueue.length) {
		/// TODO: Check if the oldest item is too close to the present, and
		/// delay it by a couple a seconds if so. At least delay things by the
		/// duration of an API roundtrip, to prevent out-of-order messages
		/// when streaming stuff.

		// 		masterVerbose("Waiting for item at ", itemQueue.peek().timestamp, Date.now() - itemQueue.peek().timestamp, 'to resolve');

		const top = itemQueue.peek().itemPromise;
		top.then((i) => {
			if (top !== itemQueue.peek().itemPromise) {
				// The top item changed while waiting for it to resolve, recurse.
				return popOldest();
			} else {
				// 				masterVerbose("Popping item at ", itemQueue.peek().timestamp, Date.now() - itemQueue.peek().timestamp);

				const popped = itemQueue.pop();
				onItemReady(i, popped.timestamp);
				// 				popped.
				// 				popped.itemPromise.then((i)=>onItemReady(i, popped.timestamp));
				// 		popped.itemPromise.then(onItemReady);

				return popOldest();
			}
		});

		// 		masterVerbose("Popping item at ", itemQueue.peek().timestamp, Date.now() - itemQueue.peek().timestamp);
		//
		// 		const popped = itemQueue.pop();
		// 		popped.itemPromise.then((i)=>onItemReady(i, popped.timestamp));
		// // 		popped.itemPromise.then(onItemReady);
		// 	} else {
		// 		masterVerbose('Queue empty.');
	}
}

// stringifies and displays the item.
function onItemReady(item, timestamp) {
	if (!item) {
		// If there is no item, this means that a synthetic entry was added
		// to the queue - most probably to block the queue temporarily.

		// 		masterVerbose("Popped empty item. ", timestamp, Date.now() - timestamp, "now " + itemQueue.length + ' items pending.');

		return;
	}

	// 	masterVerbose(
	// 		"Popped item:",
	// 		JSON.stringify({ timestamp: item.timestamp, str: item.str }),
	// 		timestamp, Date.now() - timestamp, "now " + itemQueue.length + ' items pending.'
	// 	);

	let id = circularBuffer.push(item);
	item.soclialId = id;

	// 	console.log('onItemReady, item accountNumber is ', item.accountNumber);
	// 	console.log('onItemReady, all accounts config is ', conf.all.accounts);
	// 	console.log('onItemReady, account config is ', conf.all.accounts[item.accountNumber]);
	const accConf = conf.all.accounts[Number(item.accountNumber)];
	// 	let badge = conf.all.accounts[Number(item.accountNumber)].badge; // What's the badge for this platform/account??
	const badge = accConf.badge;

	const timestampColour = conf.get("global.colours.timestamp");
	const accBadgeColour = accConf.color || conf.get("global.colours.badge");
	const channelBadgeColour =
		accConf.channels[item.channel].color || accBadgeColour;

	let header =
		"[" +
		chalkPipe(timestampColour)(formattedTimestamp(item.timestamp)) +
		"] " +
		id +
		"> " +
		chalkPipe(accBadgeColour)(badge + "/") +
		chalkPipe(channelBadgeColour)(item.channel);

	let formatted = stringify(conf, item);

	vorpal.log(header + formatted);

	// 	popOldest(); // Go to the next one.
}

// Iterate through the platforms/accounts/channels in the config, and go enabling them.

masterLog("Initializing social accounts...");

// These define the state of the application in this global scope.
let activeAccounts = {};

function activateAccount(accId) {
	// 	console.log(accId, conf.all.accounts[accId]);

	let accConf = conf.all.accounts[accId];
	let pl = accConf.platform;
	let badge = accConf.badge;

	masterLog(
		"Activating account",
		accId,
		"represented by badge",
		badge,
		"platform",
		pl,
		"..."
	);

	if (!(accConf.platform in platforms)) {
		masterError("Unknown platform: ", pl);
		throw new Error(
			"Unknown platform in a configured account. Please edit your configuration manually to remove the offending account."
		);
	}

	// Instantiate the account, passing a plain object containing all the needed API
	// surface for an account to interact with the master soclial.
	activeAccounts[accId] = new platforms[pl].Account(
		{
			emit: handle,
			verbose: getLoggerForColours("verbose", badge + ">>"),
			log: getLoggerForColours("system", badge + ">>"),
			error: getLoggerForColours("error", badge + ">>"),
			prompt: getLoggerForColours("prompt", badge + ">>"),
			accId: accId,
			badge: badge,
		},
		accConf
	);

	for (let ch in accConf.channels) {
		masterLog(
			"Listening to channel ",
			ch,
			":",
			JSON.stringify(accConf.channels[ch]),
			"..."
		);
		activeAccounts[accId].listen(ch /*accConf.channels[ch]*/);
	}
}

for (let accId in conf.all.accounts) {
	activateAccount(accId);
}

// Command: config list
vorpal
	.command("config list", "Lists the current configuration values.")
	.action(function(args, callback) {
		masterLog("Listing current configuration:");
		let all = flatten(conf.all);
		for (let i in all) {
			masterLog("    ", i, ": ", all[i]);
		}

		callback();
	});

// Command: config set
vorpal
	.command("config set <key> <value>", "Sets a configuration value.")
	.action(function(args, callback) {
		if (args.key === undefined || args.value === undefined) {
			masterError("Specify configuration key and value to be set.");
		} else {
			masterLog(
				"Setting configuration value ",
				args.key,
				": ",
				conf.get(args.key),
				" → ",
				args.value
			);
			conf.set(args.key, args.value);
		}

		callback();
	});

// Command: config delete
vorpal
	.command("config delete <key>", "Deletes a configuration value.")
	.action(function(args, callback) {
		if (args.key === undefined) {
			masterError("Specify configuration key to be deleted.");
		} else {
			masterLog(">>> Deleting configuration value ", args.key);
			conf.delete(args.key);
		}

		callback();
	});

// Command: accounts list
vorpal
	.command("accounts list", "Lists the accounts registered.")
	.action(function(args, callback) {
		let out = ["Listing registered accounts:"];

		for (let accId in conf.all.accounts) {
			let accConf = conf.all.accounts[accId];
			let pl = accConf.platform;
			let badge = accConf.badge;

			out = out.concat([
				accId,
				", represented by badge ",
				badge,
				", platform ",
				pl,
				"",
			]);
		}

		masterLog.apply(out);
		callback();
	});

// Command: accounts delete
vorpal
	.command(
		"accounts delete <id>",
		"Removes an account given its soCLIal numerical ID."
	)
	.action(function(args, callback) {
		if (args.id === undefined) {
			masterError("Specify platform and alias of account to be deleted.");
		} else {
			let flat = flatten(conf.all.accounts[args.id]);
			for (let i in flat) {
				conf.delete("accounts." + args.id + "." + i);
			}

			/// TODO: Unlisten to all channels
			/// TODO: Destroy instance

			masterLog("Account deleted. Restart soCLIal to apply changes.");
		}
		callback();
	});

// Command: accounts add
vorpal
	.command(
		"accounts add",
		"Adds a new account. You will be asked questions about it."
	)
	.action(function(args, callback) {
		vorpal.ui.cancel();

		inquirer
			.prompt([
				{
					type: "list",
					name: "platform",
					message:
						"You are adding a new account to soCLIal. From which social platform?",
					choices: Object.keys(platforms),
				},
			])
			.then((answer) => {
				// 			vorpal.ui.redraw();
				masterLog(
					"Asking the platform code to perform the sign-up procedure"
				);

				return platforms[answer.platform].signUp().then((opts) => {
					masterLog(
						"Platform code says we can create a new account with data: ",
						opts
					);

					// 				vorpal.ui.cancel();

					return inquirer
						.prompt([
							{
								type: "input",
								name: "badge",
								message:
									"What will be the badge of this account? Press enter to use the default.",
								choices: Object.keys(platforms),
								default: platforms[answer.platform].defaultBadge,
							},
						])
						.then((answer2) => {
							opts.badge = answer2.badge;
							opts.platform = answer.platform;

							// Find the lowest unused account ID
							let accId = 1;
							while (accId in conf.all.accounts) {
								accId++;
							}

							masterLog(
								"Should store config for a new account: ",
								opts
							);

							let flat = flatten(opts);

							masterLog(conf.all.accounts);
							masterLog("flat account config: ", flat);

							for (let i in flat) {
								conf.set("accounts." + accId + "." + i, flat[i]);
								this.log(
									"accounts." + accId + "." + i + " = " + flat[i]
								);
							}

							masterLog("New account added to config. Enabling now.");

							activateAccount(accId);

							callback();
						});
				});
			})
			.catch((err) => {
				masterError("Creation of new account cancelled ", err);
				// 			ui.redraw.done()
				callback();
			});
	});

// Command: item url
vorpal
	.command(
		"url <id>",
		"Displays the unequivocal URL(s) for an item, given its soCLIal letter-letter-number ID."
	)
	.action(function(args, callback) {
		if (args.id === undefined) {
			masterError('Specify item ID, e.g. "url ac7".');
		} else {
			let item = circularBuffer.get(args.id);

			if (!item) {
				masterError("Item ID doesn't exist.");
			} else {
				masterLog("URL for", args.id, "is", item.url);
				if (item.echoed) {
					masterLog("URL for echoed", args.id, "is", item.echoed.url);
					if (item.echoed.quoted) {
						masterLog(
							"URL for quoted",
							args.id,
							"is",
							item.echoed.quoted.url
						);
					}
				}
				if (item.quoted) {
					masterLog("URL for quoted", args.id, "is", item.quoted.url);
				}
			}
		}
		callback();
	});

// Command: post something
vorpal
	.command("post [str...]")
	.description("Post something to all platforms at once.")
	.action((args, callback) => {
		let str = args.str.join(" ");
		masterVerbose("Should post: ", args.str.join(" "));
		// 		this.cancel();

		for (let i in activeAccounts) {
			activeAccounts[i].post(str);
		}

		callback();
	});

// Command: reply to something
vorpal
	.command("reply <id> [str...]")
	.description("Post a reply to an item.")
	.action((args, callback) => {
		if (args.id === undefined) {
			masterError("Specify item ID, e.g.", "url ac7");
		} else {
			let item = circularBuffer.get(args.id);
			if (!item) {
				masterError("Specified item ID ", args.id, " does not exist");
				return;
			}
			let str = args.str.join(" ");

			masterVerbose("Should reply with: ", args.str.join(" "));
			// 		this.cancel();

			activeAccounts[Number(item.accountNumber)].reply(item, str);
		}
		callback();
	});

// Command: echo something
vorpal
	.command("echo <id>")
	.description("Echo (RT/boost/reshare) an item, in all platforms at once.")
	.action((args, callback) => {
		if (args.id === undefined) {
			masterError('Specify item ID, e.g. "url ac7".');
		} else {
			let item = circularBuffer.get(args.id);
			if (!item) {
				masterError("Specified item ID " + args.id + " does not exist");
				return;
			}

			let fallbackStr = activeAccounts[Number(item.accountNumber)].echoify(
				item
			);
			masterVerbose("String for fallback echo is: ", fallbackStr);

			for (let i in activeAccounts) {
				activeAccounts[i].echo(item, fallbackStr);
			}
		}
		callback();
	});

masterPrompt("", "Welcome to soCLIal !!!");
masterPrompt("Type", "help", "to see available commands.");

// Init main loop.
vorpal.delimiter("soCLIal> ").show();
