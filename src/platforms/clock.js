/*
 * This is a demo platform module.
 *
 * Every platform is a ES2015 module which must expose:
 * - Account (a subclass of 'Account'; a bit confusing, I know)
 * - name (a String)
 * - defaultBadge (a short String)
 * - signUp (a function that must return a Promise to a set of config options
 *      for a new configured account)
 */

import Account from "../abstracts/account";

import inquirer from "inquirer";

// 🍂class ClockAccount
// 🍂inherits Account
// A dumb platform which displays timestamps.

class ClockAccount extends Account {
	constructor(master, opts) {
		super(master, opts);

		this._timers = {};
	}

	get defaults() {
		return this._defaults;
	}

	// 🍂channel day; Ticks once every day
	// 🍂channel hour; Ticks once every hour
	// 🍂channel quarter; Ticks once every 15 minutes
	// 🍂channel minute; Ticks once every minute
	// 🍂channel second; Ticks once every second
	listen(channel) {
		let delay = Number(channel) * 1000;

		if (channel === "day") {
			delay = 1000 * 3600 * 24;
		}
		if (channel === "hour") {
			delay = 1000 * 3600;
		}
		if (channel === "quarter") {
			delay = 1000 * 60 * 15;
		}
		if (channel === "minute") {
			delay = 1000 * 60;
		}
		if (channel === "second") {
			delay = 1000;
		}

		if (delay <= 0) {
			return;
		}

		this._scheduleNextTickItem(channel, delay);

		return this;
	}

	_scheduleNextTickItem(channel, interval) {
		// Using a Date.now() diff instead of a setInterval() to avoid time drifting.
		let msSinceLastTick = Date.now() % interval;

		this._timers[channel] = setTimeout(() => {
			this._emitTickItem(channel);
			this._scheduleNextTickItem(channel, interval);
		}, interval - msSinceLastTick);
	}

	_emitTickItem(channel) {
		const now = Date.now();
		this.emit(
			now,
			Promise.resolve({
				str: "Tick.",
				channel: channel,
				timestamp: now,
			})
		);
	}

	unlisten(channel) {
		channel = Number(channel);
		if (channel <= 0) return;

		cancelTimeout(this._timers[channel]);
		delete this._timers[channel];

		return this;
	}

	listChannels() {
		return ["day", "hour", "quarter", "minute"];
	}
}

// signUp returns a Promise for a set of options. If the promise resolves, these options
// will be saved into the user preferences, then a new instance of this platform's `Account`
// will be instantiated.
export async function signUp() {
	console.log('Performing sign-up procedure for a new "clock" account');

	/// TODO: Extend this with a prompt for the default channel to listen to, or a prompt for
	/// the badge.

	const answer = await inquirer.prompt([
		{
			type: "list",
			name: "confirm",
			message: 'You are going to add a "clock" fake platform. Is this right?',
			choices: ["yes", "no"],
		},
	]);

	if (answer.confirm === "yes") {
		// The clock platform does not need any parameters to initialize
		// an account. In a normal case, this response would include
		// auth tokens and so on.
		return {
			channels: { quarter: { color: "grey" }, hour: { color: "red" } },
			color: "grey",
		};
	} else {
		throw new Error("Not creating the new clock account then.");
	}
}

export const defaultBadge = "cl"; // or '⏰'
export const name = "clock";
export { ClockAccount as Account };
