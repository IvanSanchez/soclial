import Account from "../abstracts/account";

import inquirer from "inquirer";
import { OAuth2 } from "oauth";
import chalk from "chalk-pipe";
import Mastodon from "mastodon-api";
import striptags from "striptags";
import he from "he";
import TimeLock from "../timelock";

const disabled = true;

// 🍂class MastodonAccount
// 🍂inherits Account
// Support for the Mastodon microblogging federated network.

class MastodonAccount extends Account {
	constructor(master, opts) {
		super.constructor(master, opts);
		// 		this._timers = {};

		/// FIXME: DEBUG!!!!
		// 		return;

		this._opts = Object.assign(
			{
				// 			colour: 'grey'
			},
			opts
		);

		this._masto = new Mastodon({
			access_token: opts.access_token,
			api_url: opts.api_url,
		});

		if (disabled) return;

		this._vorpal = vorpal;
		this._accountNumber = accountNumber;

		let initialTimeLock = new TimeLock();
		this.emit(-Infinity, initialTimeLock.promise);

		this._masto
			.get("accounts/verify_credentials")
			/*.then(response=>response.json()).*/
			.then((json) => {
				// 			console.log(chalk.purple('>>> Mastodon account credentials: '), json);
				vorpal.log(
					chalk.purple("You are logged in the Mastodon platform as "),
					chalk.cyan(json.data.username),
					chalk.purple(" in server "),
					chalk.cyan(
						opts.api_url.replace(/.*:\/\//, "").replace(/\/.*/, "")
					),
					chalk.purple(", account badge is "),
					chalk.cyan(opts.badge)
				);

				this._id =
					json.data.username +
					"@" +
					opts.api_url.replace(/.*:\/\//, "").replace(/\/.*/, "");

				// Hackishly listen to the public timeline
				let userListener = this._masto.stream("streaming/user");
				userListener.on("message", this._getOnStreamMessage("user"));
				userListener.on("error", (err) => {
					vorpal.log(chalk.red.bold(err));
				});

				let homeTimeLock = new TimeLock();
				this.emit(-Infinity, homeTimeLock.promise);
				let notificationsTimeLock = new TimeLock();
				this.emit(-Infinity, notificationsTimeLock.promise);

				this._masto.get("timelines/home", (err, msgs, response) => {
					for (let i = msgs.length - 1; i >= 0; i--) {
						this._normalizeAndEmitUpdate("home", msgs[i]);
						// 					vorpal.log('timelines/home', msgs);
					}
					homeTimeLock.unlock();
				});

				this._masto.get("notifications", (err, msgs, response) => {
					// 				vorpal.log('notifications', err, msgs/*, response*/);
					// 				vorpal.log(response.toJSON());

					for (let i = msgs.length - 1; i >= 0; i--) {
						this._normalizeAndEmitNotification("notifications", msgs[i]);
						// 					vorpal.log('timelines/notifications', msgs);
					}
					notificationsTimeLock.unlock();
				});

				// 			let publicListener = this._masto.stream('streaming/public');
				// 			publicListener.on('message', this._getOnStreamMessage('public'));
				// 			publicListener.on('error', err => {
				// 				vorpal.log(chalk.red.bold(err));
				// 			});

				initialTimeLock.unlock();
			})
			.catch((err) => {
				this._vorpal.log(
					chalk.red(">>> Could not log into Mastodon account.")
				);
				this._vorpal.log(chalk.red(">>> " + err));
			});
	}

	listen(channel) {
		/// FIXME!!!
	}

	unlisten(channel) {
		/// FIXME!!!
	}

	listChannels() {
		// 		return ['home', 'notifications', 'public'];
	}

	post(str) {
		this._post({ status: str });
	}

	reply(item, str) {
		let replyStatusId = item.raw.id;
		let replyStatusScreenName = "@" + item.raw.account.acct;

		if (str.toLowerCase().indexOf(replyStatusScreenName.toLowerCase()) === -1) {
			/// FIXME!!! This will match partial usernames (e.g. searching for @john
			/// will match @johnDoe). Should use some kind of regexp here.

			str = replyStatusScreenName + " " + str;
		}

		return this._post({
			status: str,
			in_reply_to_id: replyStatusId,
		});
	}

	echoify(item) {
		return "Boost @" + item.raw.account.acct + ": " + item.str;
	}

	echo(item, str) {
		if (item.platform === "mastodon") {
			// Native boost
			return this._masto.post("statuses/" + item.raw.id + "/reblog");

			// 		.then((error, toot, response)=>{
			// 			if(error) throw error;
			// 			// 			console.log(toot);  // Toot body.
			// 			// 			console.log(response);  // Raw response object.
			// 		});
		} else {
			let totalLength = str.length + 1 + item.url.length;
			if (totalLength > 500) {
				// Time to clip something, and replace rest with an ellipsis
				str = str.substr(0, 495 - item.url.length) + "…";
			}
			str += " " + item.url;

			return this.post(str);
		}
	}

	// Common to post, reply, replyall, echo
	_post(payload) {
		if (disabled) return;

		this._masto.post("statuses", payload).catch((err) => {
			this._vorpal.log(
				chalk.red(">>> Could not post to twitter: " + error[0].message)
			);
		});
		// 		.then((error, toot, response)=>{
		// 			if(error) throw error;
		// 			// 			console.log(toot);  // Toot body.
		// 			// 			console.log(response);  // Raw response object.
		// 		});
	}

	_cleanString(str) {
		return striptags(he.decode(str.replace(/<\/p><p>/g, " // ")));
	}

	// Given a stream descriptor, return a function that handles messages from the stream
	_getOnStreamMessage(stream) {
		return (msg) => {
			// 			this._vorpal.log(chalk.green(msg));
			if (msg.event === "update") {
				this._normalizeAndEmitUpdate(stream, msg.data);
			} else if (msg.event === "notification") {
				this._normalizeAndEmitNotification(stream, msg.data);
			} else {
				this._vorpal.log(chalk.green(msg));
			}
		};
	}

	// Get a Mastodon update, normalizes some stuff (to make different platforms
	// see the same data from different platforms)
	_normalize(channel, msg) {
		// 		console.log('normalizing: ', msg);

		/// FIXME!!!
		if (msg.spoiler_text) {
			this._vorpal.log("Spoiler: ", msg.spoiler_text);
		}

		return {
			accountNumber: this._accountNumber,
			accountId: this._id,
			platform: "mastodon",
			sender: msg.account.acct,
			str: msg.reblog ? "" : this._cleanString(msg.content),
			url: msg.url,
			channel: channel,
			// 			realtime: true,
			media: msg.media_attachments.map((i) => i.text_url || i.remote_url),
			timestamp: Date.parse(msg.created_at),
			echoed: msg.reblog && this._normalize(channel, msg.reblog),
			raw: msg,
		};

		// 				if (msg.mentions) {
		// 					vorpal.log('Mentions: ', msg.mentions);
		// 				}
		// 				if (msg.media_attachments.length) {
		// 					this._vorpal.log('Media: ', msg.media_attachments);
		// 				}
		// 				if (msg.tags) {
		// 					vorpal.log('Tags: ', msg.tags);
		// 				}

		// 		if (msg.reblog) {
		// 			this._vorpal.log('FIXME: This is a boost of another status');
		// 			this._normalizeAndEmitUpdate(channel, msg.reblog);
		// 		}
	}

	_normalizeAndEmitUpdate(channel, msg) {
		let normalized = this._normalize(channel, msg);

		if (normalized) {
			this.emit(normalized.timestamp, Promise.resolve(normalized));
		}
	}

	_normalizeAndEmitNotification(channel, msg) {
		let timestamp = Date.parse(msg.created_at);

		if (msg.type === "follow") {
			this.emit(
				timestamp,
				Promise.resolve({
					accountNumber: this._accountNumber,
					accountId: this._id,
					str: chalk.bold(msg.account.acct) + " started following you.",
					channel: channel,
					timestamp: timestamp,
				})
			);
		} else if (msg.type === "reblog") {
			this.emit(
				timestamp,
				Promise.resolve({
					accountNumber: this._accountNumber,
					accountId: this._id,
					str:
						chalk.bold(msg.account.acct) +
						" echoed your update: " +
						this._cleanString(msg.status.content),
					channel: channel,
					timestamp: timestamp,
				})
			);
		} else if (msg.type === "favourite") {
			this.emit(
				timestamp,
				Promise.resolve({
					accountNumber: this._accountNumber,
					accountId: this._id,
					str:
						chalk.bold(msg.account.acct) +
						" favourited your update: " +
						this._cleanString(msg.status.content),
					channel: channel,
					timestamp: timestamp,
				})
			);
		} else if (msg.type === "mention") {
			this.emit(
				timestamp,
				Promise.resolve({
					accountNumber: this._accountNumber,
					accountId: this._id,
					str:
						chalk.bold(msg.account.acct) +
						" mentioned you " +
						this._cleanString(msg.status.content),
					channel: channel,
					timestamp: timestamp,
				})
			);
		} else {
			this._vorpal.log("channel", msg);
		}
	}
}

// signUp returns a Promise for a set of options. If the promise resolves, these options
// will be saved into the user preferences, then a new instance of this platform's `Account`
// will be instantiated.
export function signUp() {
	/// TODO: Prompt the user for a server.
	let server = "mastodon.social";
	/// TODO: Do some automatic client ID/secret fetching, depending on the server.
	let clientId =
		"aa0b5f7df451a9f9cfeff58e103ee5de4b6f724f4a576f71f7e81a87fb255722";
	let clientSecret =
		"3736a19af63f85ed75d72caa7ec09661ba8ed38303c178316cbe927d38233f7e";

	let oauth = new OAuth2(
		clientId,
		clientSecret,
		"https://" + server,
		null,
		"/oauth/token"
	);
	let url = oauth.getAuthorizeUrl({
		redirect_uri: "urn:ietf:wg:oauth:2.0:oob",
		response_type: "code",
		scope: "read write follow",
	});
	// Get the user to open up the url in their browser and get the code

	this.log(
		"In order to add a new Mastodon account, visit",
		url,
		"to get a verification code."
	);

	return inquirer
		.prompt([
			{
				type: "input",
				name: "code",
				message: "Enter verification code: ",
			},
		])
		.then((answer) => {
			if (!answer.code) {
				throw new Error("No verification code was introduced.");
			}

			return new Promise((resolve, reject) => {
				oauth.getOAuthAccessToken(
					answer.code,
					{
						grant_type: "authorization_code",
						redirect_uri: "urn:ietf:wg:oauth:2.0:oob",
					},
					function(err, accessToken, refreshToken, res) {
						if (err) {
							return reject(err);
						}

						return resolve({
							// 			server: server,
							// 			clientId: clientId,
							// 			clientSecret: clientSecret,
							api_url: "https://" + server + "/api/v1/",
							access_token: accessToken,
						});
					}
				);
			});
		});
}

export const defaultBadge = "ma"; // or '🐘'
export const name = "mastodon";
export { MastodonAccount as Account };
