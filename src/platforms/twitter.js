import Account from "../abstracts/account";
import TimeLock from "../timelock";

import inquirer from "inquirer";
import { OAuth } from "oauth";
import Twitter from "twitter";
// import striptags from 'striptags';
// import he from 'he';
import fs from "fs";

const soclialConsumerKey = "xiUNN9g7K8it8QIXcA";
const soclialConsumerSecret = "qsFwbc6gXtcHAplAXjfoKXu96hVXz82W7sEbzuLR2U";

// 🍂class TwitterAccount
// 🍂inherits Account
// Support for the Twitter microblogging network.

class TwitterAccount extends Account {
	constructor(master, opts) {
		super(master, opts);

		const { emit, log, error, verbose, prompt, accId, badge } = master;

		// 		/// While we are debugging, it's a good idea to not ask too much to the twitter servers.
		// 		return;	/// DEBUG!!!

		this._opts = Object.assign(
			{
				// 			colour: 'grey'
			},
			opts
		);

		this._tw = new Twitter({
			consumer_key: soclialConsumerKey,
			consumer_secret: soclialConsumerSecret,
			access_token_key: opts.clientId,
			access_token_secret: opts.clientSecret,
		});

		this.log = log;
		this._error = error;
		this._verbose = verbose;
		this._accountNumber = accId;

		this._lenghtOfTCoLinks = 30; // Start with a conservative value

		let initialTimeLock = new TimeLock();

		this._listening = {};
		this._lastStatusesPerChannel = {};
		this._rateLimits = { resources: {} };
		this._rateLimitResets = {}; // map timestamps to promises of scheduled updates

		// Lock the stream until we have some data from all the channels
		this.emit(-Infinity, initialTimeLock.promise);
		// 		console.log('TW platform locked the main stream on user init');

		Promise.all([
			this._getEndpointPromise("account/verify_credentials", Infinity),
			this._refreshRateLimits(),
		]).then(([credentials]) => {
			// 			rateLimits
			log(
				"You are logged in the Twitter platform as ",
				credentials.screen_name,
				", full name is ",
				credentials.name,
				", account badge is ",
				opts.badge
			);
			log(
				"You follow",
				credentials.friends_count,
				"accounts, and have",
				credentials.followers_count,
				"followers; you appear on",
				credentials.listed_count,
				"lists and have tweeted",
				credentials.statuses_count,
				"times."
			);

			fs.writeFile(
				"debug/twitter-credentials.json",
				JSON.stringify(credentials, null, " "),
				(err) => {
					if (err) {
						error(
							"Could not write debug twitter credentials response:",
							err
						);
					}
				}
			);

			this._id = credentials.screen_name + "@twitter";

			this._idStr = credentials.id_str;

			// Hackishly listen to the user's main stream
			// TODO: Split this out, set a flag when streaming is working
			// TODO: Handle errors when the network connectivity changes
			// TODO: Handle errors when receiving a HTTP status response of 420 (connecting too often, or too many clients)
			// 			this._stream = this._tw.stream('user', {
			// // 				replies: "all",	// Should explicitly listen to the "replies" channel
			// // track: "foo,bar"	// Should use this for listening to search terms channels
			// 				stall_warnings: true,
			//                 track: '#foss4g,#jsgeo' /// FIXME: Should read from activated channels
			// 			}, (stream)=>{
			// 				stream.on('data', (ev)=>{
			// // 					vorpal.log(ev);
			// 					this._normalizeAndEmitUpdate(ev, 'stream');
			// 				});
			//
			// 				stream.on('error', function(error) {
			// 					this._error(error);
			// 				});
			//
			// 			});

			// 			// Hackishly listen to the "samples" public timeline
			// 			this._stream = this._tw.stream('statuses/sample', {
			// // 				replies: "all",	// Should explicitly listen to the "replies" channel
			// 				// track: "foo,bar"	// Should use this for listening to search terms channels
			// 				stall_warnings: true
			// 			}, (stream)=>{
			// 				stream.on('data', (ev)=>{
			// // 					vorpal.log(ev);
			// 					this._normalizeAndEmitUpdate(ev, 'public');
			// 				});
			//
			// 				stream.on('error', function(error) {
			// 					this._error((error));
			// 				});
			// 			});

			// 			this.listen("home");
			// 			this.listen("mentions");

			// 			let searchTimelineLock = new TimeLock();
			// 			this.emit(-Infinity, searchTimelineLock.promise);
			// 			// Get a few of the last statuses containing "foss4g"
			// 			this._tw.get('search/tweets', {
			// 				count: 20,
			//
			// 				// This undocumented option makes the tweets have the full text,
			// 				// and never be truncated.
			// // 				tweet_mode: 'extended',
			//                 result_type: 'recent',
			//                 q: 'foss4g'
			// // 				count: 10
			// 			}, (err, msgs, response)=>{
			// 				if (err) {
			// 					this._error((err));
			// 				}
			// 				// console.log(msgs.search_metadata);
			// 				for (let i=msgs.statuses.length-1; i>=0; i--) {
			// // 					console.log(msgs.statuses[i]);
			// 					this._normalizeAndEmitUpdate(msgs.statuses[i], 'foss4g');
			// // 					vorpal.log('timelines/home', msgs);
			// 				}
			// // 				console.log('TW removed home timeline lock');
			// 				searchTimelineLock.unlock();
			// 			});

			initialTimeLock.unlock();

			// Fetches some "constants" from the API.
			// See https://dev.twitter.com/rest/reference/get/help/configuration
			this._tw.get("help/configuration", {}, (err, json) => {
				if (err) {
					this._error(err);
				}
				this._lenghtOfTCoLinks = json.short_url_length_https;

				fs.writeFile(
					"debug/twitter-config.json",
					JSON.stringify(json, null, " "),
					(err) => {
						if (err) {
							this._error(
								"Could not write debug twitter config: ",
								err
							);
						}
					}
				);

				return this.log(
					"Length of t.co links in the twitter platform is currently ",
					this._lenghtOfTCoLinks
				);
			});
		});
	}

	// Internal. Sets a timeout to request updates from one specific channel, as soon as to not
	// exceed the rate limits, handling timelocks and such.
	// `lastStatus` will be fed as an API option.
	// `repeat` will trigger queuing the same request after this is done
	// `immediate` will skip the calculation for the rate limit delay
	_requestChannelOnce(channel, immediate = false) {
		let timeLock = new TimeLock();
		this.emit(Date.now(), timeLock.promise);
		let opts = {}; // Options for the API request. We'll add stuff depending on the channel.
		let endpoint; // Which API endpoint to use, also defines the rate limit query
		let rateLimitGroup; // Looks like the first part of the endpoint

		if (channel === "home") {
			rateLimitGroup = "statuses";
			endpoint = "statuses/home_timeline";
			opts.count = 100;

			// This not-clearly-documented option makes the tweets have the full text,
			// and never be truncated.
			opts.tweet_mode = "extended";
		} else if (channel === "mentions") {
			rateLimitGroup = "statuses";
			endpoint = "statuses/mentions_timeline";
			opts.count = 100;

			// This not-clearly-documented option makes the tweets have the full text,
			// and never be truncated.
			opts.tweet_mode = "extended";
		} else if (channel.match(/^search\//)) {
			rateLimitGroup = "statuses";
			endpoint = "statuses/mentions_timeline";
			opts.count = 100;

			// This not-clearly-documented option makes the tweets have the full text,
			// and never be truncated.
			opts.tweet_mode = "extended";
		} else {
			this.error("Cannot get endpoint for given channel", channel);
		}

		if (this._lastStatusesPerChannel[channel]) {
			opts.since_id = this._lastStatusesPerChannel[channel];
		}

		// Calculate the delay based on the rate limits
		// 		let limit = this._getRateLimits(rateLimitGroup, endpoint);

		// 		let millisecondsToReset = limit.reset * 1000 - Date.now();

		/// FIXME
		return this._waitRateLimit(rateLimitGroup, endpoint, immediate).then(() => {
			if (!this._listening[channel]) {
				timeLock.unlock();
				throw new Error();
			}

			this._tw.get(endpoint, opts, (err, msgs, response) => {
				if (err) {
					if (typeof err === "string") {
						this._error("Error while requesting ", endpoint, ":", err);
					} else if (err instanceof Array) {
						err.forEach(er=>{
						this._error("Error while requesting ", endpoint, ": code", er.code, " // ", er.message);
						});
					}

				}

				if (response) {
					// Update rate limits based on HTTP headers from the API server
					// TODO: Check if this equals the expected limit, and if not,
					// agressively throttle down the expected remaining limit.
					if (!this._rateLimits.resources[rateLimitGroup]) {
						this._rateLimits.resources[rateLimitGroup] = {};
					}
					if (
						!this._rateLimits.resources[rateLimitGroup]["/" + endpoint]
					) {
						this._rateLimits.resources[rateLimitGroup][
							"/" + endpoint
						] = {};
					}
					if (response.headers["x-rate-limit-remaining"]) {
						this._rateLimits.resources[rateLimitGroup][
							"/" + endpoint
						].remaining = Number(
							response.headers["x-rate-limit-remaining"]
						);
					}
					if (response.headers["x-rate-limit-reset"]) {
						this._rateLimits.resources[rateLimitGroup][
							"/" + endpoint
						].reset = Number(response.headers["x-rate-limit-reset"]);
					}

					// 						this.log(
					// 							'Got data from',
					// 							rateLimitGroup + '/' + endpoint,
					// 							'at',
					// 							this._formatTimestamp(Date.now()),
					// 							', updated rate limit is',
					// 							this._rateLimits.resources[rateLimitGroup]['/' + endpoint].remaining
					// 						);

					if (msgs.statuses) {
						msgs = msgs.statuses;
					}

					for (let i = msgs.length - 1; i >= 0; i--) {
						// 					console.log(msgs[i]);
						this._normalizeAndEmitUpdate(msgs[i], channel);
						// 					this._vorpal.log('timelines/home', msgs);
					}

					if (msgs[0]) {
						this._lastStatusesPerChannel[channel] = msgs[0].id_str;
					}
					this._requestChannelOnce(channel);
				} else {
					// No response? Maybe there's a network problem. Retry in 10 seconds.
					setTimeout(() => {
						this._requestChannelOnce(channel);
					}, 10000);
				}
				// 				console.log('TW removed home timeline lock');
				timeLock.unlock();

				// ...and loop ad aeterna.
				return;
			});
		});
	}

	// Internal, logging. Given a timestmp from a limit structure, convert its timestamp to a human-readable timestamp.
	_formatTimestamp(timestamp) {
		const startTime = new Date();
		const timezoneOffset = startTime.getTimezoneOffset();
		timestamp -= timezoneOffset * 60000;

		// https://stackoverflow.com/questions/10645994/node-js-how-to-format-a-date-string-in-utc
		const date = new Date(timestamp);
		// 		this._vorpal.log(
		// 			chalk.purple('>>> Date for timestamp '),
		// 			chalk.cyan(timestamp),
		// 			chalk.purple('is'),
		// 			chalk.cyan(date)
		// 		);
		// 		try {
		return date
			.toISOString()
			.replace(/T/, " ") // replace T with a space
			.replace(/\..+/, ""); // delete the dot and everything after
		// 		} catch(e) {
		// 			return "Aaaiiiieeeee";
		// 		}
	}

	// Internal, gets the {limit:..., remaining:..., reset:...} rate limit for the given
	// resource group and resource endpoint
	// e.g. _getRateLimits('statuses', 'statuses/home_timeline')
	// This will also schedule a refresh of the rate limits when this limit should
	// be reset, if needed.
	_getRateLimits(group, endpoint) {
		if (
			!(
				this._rateLimits.resources &&
				this._rateLimits.resources[group] &&
				this._rateLimits.resources[group]["/" + endpoint]
			)
		) {
			// If there is no known rate limit, return a non-rate-limited fake,
			// which should make things wait one minute.
			return { limit: 20, remaining: 10, reset: Date.now() / 1000 + 60 };
		}

		let limit = this._rateLimits.resources[group]["/" + endpoint];
		let reset = limit.reset;

		/// TODO: re-request the rate limits when the reset time comes.
		if (!this._rateLimitResets[limit.reset]) {
			this._rateLimitResets[limit.reset] = new Promise((resolve) => {
				let millisecondsToReset = limit.reset * 1000 - Date.now() + 2000;
				// 				this._error(
				// 					"Reset timestamp for rate limit on ",
				// 					endpoint,
				// 					"reached, will re-request all rate limits at",
				// 					this._formatTimestamp(limit.reset * 1000),
				// 					"sleeping for",
				// 					millisecondsToReset
				// 				);
				setTimeout(() => {
					resolve(this._refreshRateLimits());
				}, millisecondsToReset);
			});
		}

		return limit;
	}

	_decreaseRateLimits(group, endpoint) {
		if (this._rateLimits.resources && this._rateLimits.resources[group]) {
			this._rateLimits.resources[group]["/" + endpoint].remaining--;
		}
	}

	// Returns a Promise that resolves after some time.
	// The wait depends on the rate limits; will wait a proportional
	// amount of time to the remaining requests, or else will wait
	// until a reset of the rate limits.
	// If immediate is truthy, then the delay only happens if the rate limit has been
	// exceeded.
	_waitRateLimit(group, endpoint, immediate = false) {
		const rateOffset = 2; // How many requets per reset period to left unused
		this._decreaseRateLimits(group, endpoint);
		let limit = this._getRateLimits(group, endpoint);
		let delay = 0;
		let millisecondsToReset = limit.reset * 1000 - Date.now();

		if (limit.remaining < rateOffset) {
			if (millisecondsToReset > 0) {
				// Wait until the next rate reset
				/// TODO: Maybe this could be better if made into a recursive call??

				this._error(
					"Twitter rate limits exceeded for ",
					endpoint,
					", throttling down agressively. Expect delays."
				);

				// Something is wrong. Be slow, and wait until after the reset timestamp for
				// the rate limits reset for this resource.
				return this._rateLimitResets[limit.reset].then(() =>
					this._waitRateLimit(group, endpoint)
				);
			} else {
				// Negative time till rate limits reset means there's something
				// wrong when fetching the rate limits.
				this._error(
					"Rate limits reset for",
					endpoint,
					"is in the past, waiting one minute."
				);
				delay = 60000;
			}
		} else if (limit.remaining === rateOffset) {
			// Wait exactly until the next rate reset
			// 			this._vorpal.log(
			// 				chalk.purple('>>> Twitter rate limit exactly hit, waiting until next reset, at '),
			// 				chalk.cyan(this._formatTimestamp(Date.now() + millisecondsToReset))
			// 			);
			this._decreaseRateLimits(group, endpoint);
			return this._rateLimitResets[limit.reset];
		} else if (!immediate) {
			delay = millisecondsToReset / (limit.remaining - rateOffset);
		}

		return new Promise((resolve) => {
			setTimeout(resolve, delay);
		});
	}

	// Internal, refreshes the rate limits. This is called in a setInterval(..., 120000) on init.
	_refreshRateLimits() {
		return this._getEndpointPromise(
			"application/rate_limit_status",
			Infinity
		).then((rateLimits) => {
			this._rateLimits = rateLimits;

			fs.writeFile(
				"debug/twitter-ratelimits.json",
				JSON.stringify(rateLimits, null, " "),
				(err) => {
					if (err) {
						this._error("Could not write debug ratelimits: ", err);
					}
				}
			);

			return rateLimits;
		});
	}

	listen(channel) {
		this._listening[channel] = true;
		this._lastStatusesPerChannel[channel] = 0;

		this._requestChannelOnce(channel, true);
	}

	unlisten(channel) {
		/// FIXME!!!
		delete this._listening[channel];
	}

	listChannels() {
		return ["home", "mentions"];
		// 		return ['home', 'replies', 'notifications', 'public'];
	}

	post(str) {
		return this._post({ status: str });
	}

	reply(item, str) {
		let replyStatusId = item.raw.id_str;
		let replyStatusScreenName = "@" + item.raw.user.screen_name;

		if (str.toLowerCase().indexOf(replyStatusScreenName.toLowerCase()) === -1) {
			/// FIXME!!! This will match partial usernames (e.g. searching for @john
			/// will match @johnDoe). Should use some kind of regexp here.

			str = replyStatusScreenName + " " + str;
		}

		return this._post({
			status: str,
			in_reply_to_status_id: replyStatusId,
		});
	}

	echoify(item) {
		if (item.echoed) {
			return (
				"RT @" +
				item.raw.retweeted_status.user.screen_name +
				": " +
				item.echoed.str
			);
		}

		return "RT @" + item.raw.user.screen_name + ": " + item.str;
	}

	echo(item, str) {
		if (item.platform === "twitter") {
			// Native RT
			this._verbose("Retweeting ID:", item.raw.id_str);
			return this._tw.post(
				"statuses/retweet",
				{
					id: item.raw.id_str,
				},
				(error, tweet, response) => {
					if (error) {
						this._error("Could not retweet: ", error[0].message);
					}
				}
			);
		} else {
			/// TODO: Take into account that twitter shortens ALL URLs into t.co links,
			/// potentially shortening the length
			let totalLength = str.length + 1 + this._lenghtOfTCoLinks;
			if (totalLength > 140) {
				// Time to clip something, and replace rest with an ellipsis
				str = str.substr(0, 140 - this._lenghtOfTCoLinks - 2) + "…";
			}
			str += " " + item.url;

			return this.post(str);
		}
	}

	// Common to post, reply, replyall, echo
	_post(payload) {
		payload.weighted_character_count = true; // Hackto enable 280chars

		this._tw.post("statuses/update", payload, (error, tweet, response) => {
			if (error && error.length) {
				this._error("Could not tweet: ", error[0].message);
				// 				throw error;
			}
			// 			console.log(tweet);  // Tweet body.
			// 			console.log(response);  // Raw response object.
		});
	}

	// A helper to promisify calls to this._tw.get().
	_getEndpointPromise(endpoint, retries) {
		return new Promise((res, rej) => {
			this._tw.get(endpoint, {}, (err, json) => {
				if (!err) {
					res(json);
				} else if (retries !== undefined && retries > 0) {
					this._error(
						"Could not fetch data from ",
						endpoint,
						"; will retry in 30s"
					);

					setTimeout(() => {
						res(this._getEndpointPromise(endpoint, retries - 1));
					}, 30000);
				} else {
					rej(err);
				}
			});
		});
	}

	_cleanString(str, entities, inReplyTo) {
		if (entities.urls) {
			entities.urls.forEach(
				(el) => (str = str.replace(el.url, el.expanded_url))
			);
		}
		if (entities.media) {
			// Media is handled by soCLIal and should not be included in the text
			entities.media.forEach((el) => (str = str.replace(el.url, "")));
		}

		if (inReplyTo) {
			str = "[re: @" + inReplyTo + "] " + str;
		}

		// 		return striptags( he.decode( str.replace('</p><p>', ' // ') ) );
		return str
			.replace(/&amp;/g, "&")
			.replace(/&gt;/g, ">")
			.replace(/&lt;/g, "<");
	}

	// Expand the media URLs. This is not trivial because of video URLs.
	_cleanMedia(msg) {
		let rawEntities = {};

		if (!msg.entities) {
			return;
		}

		if (msg.entities && msg.entities.media) {
			msg.entities.media.forEach((el) => (rawEntities[el.id_str] = el));
		}
		if (msg.extended_entities && msg.extended_entities.media) {
			msg.extended_entities.media.forEach(
				(el) => (rawEntities[el.id_str] = el)
			);
		}

		return Object.keys(rawEntities).map((k) => {
			let ent = rawEntities[k];
			if ("video_info" in ent) {
				return ent.video_info.variants[0].url;
			}
			return ent.media_url.https || ent.media_url;
		});
	}

	// Get a Twitter update, OR a part of an update (such a retweeted_status or a
	// quoted_status), and normalize some stuff (to make different platforms
	// see the same data from different platforms) into a soclial item
	_normalize(msg, channel) {
		// 		if (('id_str' in msg) && ('contributors' in msg) && ('text' in msg)) {
		if (
			"id_str" in msg &&
			(("contributors" in msg && "text" in msg) || // Non-extended
				"full_text" in msg) // Extended
		) {
			// This looks like a tweet

			if (
				msg.quoted_status &&
				msg.retweeted_status &&
				msg.retweeted_status.quoted_status &&
				msg.retweeted_status.quoted_status.id_str ===
					msg.quoted_status.id_str
			) {
				// Delete the quoted one, as the quoted from the retweet is the same
				// and has the extended attributes
				delete msg.quoted_status;
			}

			if (msg.extended_tweet && msg.extended_tweet.full_text) {
				msg.text = msg.extended_tweet.full_text;
				msg.truncated = false;
			}

			if (msg.truncated) {
				// 				console.log('Truncated ', msg.text, ' - fetching full from id ', msg.id_str);
				return {
					timestamp: Date.parse(msg.created_at),
					itemPromise: this._tw
						.get("statuses/show/", {
							id: msg.id_str,
							tweet_mode: "extended",
						})
						.then((full) => {
							if (full.truncated) {
								// After requesting individual tweet, got it truncated also
								// 						console.log('Got full but still truncated. From: «', msg.text ,  '» into: ', full);
								console.log(
									"Got full but still truncated. ",
									msg.text
								);
								full.truncated = false;
							}
							return this._normalize(full, channel).itemPromise;
						}),
				};
			}

			if (!msg.text && msg.full_text) {
				// This looks like an extended tweet, fetched via a "tweet_mode=extended" REST API call
				msg.text = msg.full_text;
			}

			if (msg.extended_tweet) {
				// This looks like an extended tweet, fetched via a realtime stream
				// 				return this._normalize(Object.assign({}, msg.extended_tweet, msg);
				msg.text = msg.extended_tweet.full_text;
				msg.entities = msg.extended_tweet.entities;
				/// TODO: Maybe something has to be done about the entities?
			}

			if (msg.retweeted_status) {
				// Wait for normalization of retweeted, return a promise
				return {
					timestamp: Date.parse(msg.created_at),
					itemPromise: this._normalize(
						msg.retweeted_status,
						null
					).itemPromise.then((echoed) =>
						Promise.resolve({
							accountNumber: this._accountNumber,
							accountId: this._id,
							platform: "twitter",
							raw: channel && msg,
							sender: msg.user.screen_name,
							str: "",
							url:
								"https://www.twitter.com/" +
								msg.user.screen_name +
								"/status/" +
								msg.id_str,
							channel: channel,
							media: [],
							timestamp: Date.parse(msg.created_at),
							echoed: echoed,
						})
					),
				};
				// 					msg.text = msg.retweeted_status.text;
			}

			let allEntities = Object.assign(
				{},
				msg.entities || {},
				msg.extended_entities || {}
			);

			// 			let allMedia = (allEntities.media && allEntities.media.map(i=>i.media_url_https || i.media_url )) || [];

			// 			let allMedia = (allEntities.media && allEntities.media.map((i)=>{
			// 				return
			// // 					(i.video_info && i.video_info.variants[0].url) ||
			// // 					i.expanded_url ||
			// 					i.media_url_https ||
			// 					i.media_url
			// 			})) || [];

			let str = this._cleanString(
				msg.text,
				allEntities,
				msg.in_reply_to_screen_name
			);

			// Wait for normalization of quoted, return a promise
			if (msg.quoted_status) {
				return {
					timestamp: Date.parse(msg.created_at),
					itemPromise: this._normalize(
						msg.quoted_status,
						null
					).itemPromise.then((quoted) =>
						Promise.resolve({
							accountId: this._id,
							platform: "twitter",
							sender: msg.user.screen_name,
							str: str,
							url:
								"https://www.twitter.com/" +
								msg.user.screen_name +
								"/status/" +
								msg.id_str,
							channel: channel,
							raw: channel && msg,
							media: this._cleanMedia(msg),
							timestamp: Date.parse(msg.created_at),
							quoted: quoted,
						})
					),
				};
			}

			// Neither quoted nor retweeted here
			return {
				timestamp: Date.parse(msg.created_at),
				itemPromise: Promise.resolve({
					accountId: this._id,
					platform: "twitter",
					sender: msg.user.screen_name,
					str: str,
					url:
						"https://www.twitter.com/" +
						msg.user.screen_name +
						"/status/" +
						msg.id_str,
					channel: channel,
					raw: channel && msg,
					media: this._cleanMedia(msg),
					timestamp: Date.parse(msg.created_at),
				}),
			};
		} else if (msg.direct_message) {
			let sender = msg.direct_message.sender_screen_name;
			if (msg.direct_message.sender_id === this._idStr) {
				sender = "→" + msg.direct_message.recipient_screen_name;
			}

			let str = this._cleanString(
				msg.direct_message.text,
				msg.direct_message.entities
			);

			return {
				timestamp: Date.parse(msg.direct_message.created_at),
				itemPromise: Promise.resolve({
					accountId: this._id,
					platform: "twitter",
					sender: sender,
					str: str,
					// 					url: 'https://www.twitter.com/' + msg.user.screen_name + '/status/' + msg.id_str,
					channel: "DM",
					raw: channel && msg,
					media: this._cleanMedia(msg.direct_message),
					timestamp: Date.parse(msg.direct_message.created_at),
				}),
			};
		} else {
			this._error("Received something which is not a tweet nor a DM", msg);
			return {
				timestamp: Date.parse(msg.created_at),
				itemPromise: Promise.resolve(msg),
			};
		}
	}

	// Get a Twitter update, normalizes some stuff (to make different platforms
	// see the same data from different platforms), and emits the update.
	_normalizeAndEmitUpdate(msg, channel) {
		let normalized = this._normalize(msg, channel);

		if (normalized) {
			this.emit(normalized.timestamp, normalized.itemPromise);
		}

		// 		/// FIXME!!!
		// 		if (msg.spoiler_text) {
		// 			this._vorpal.log('Spoiler: ', msg.spoiler_text);
		// 		}
		//
		// 		if (msg.retweeted_status) {
		// 			this._vorpal.log('FIXME: This is a RT/echo of another status (truncated: ', msg.truncated, ')');
		// // 			this._normalizeAndEmitUpdate(channel, msg.reblog);
		// 		}
		// 		if (msg.quoted_status) {
		// 			this._vorpal.log('FIXME: This is a quote of another status');
		// // 			this._normalizeAndEmitUpdate(channel, msg.reblog);
		// 		}
	}

	_normalizeAndEmitNotification(msg) {
		// 		if (msg.type === 'follow') {
		// 			this.emit({
		// 				str: chalk.bold(msg.account.acct) + ' started following you.',
		// 				channel: channel,
		// 				timestamp: Date.parse(msg.created_at)
		// 			});
		// 		} else if (msg.type === 'reblog') {
		// 			this.emit({
		// 				str: chalk.bold(msg.account.acct) + ' echoed your update: ' + this._cleanString(msg.status.content),
		// 				channel: channel,
		// 				timestamp: Date.parse(msg.created_at)
		// 			});
		// 		} else if (msg.type === 'favourite') {
		// 			this.emit({
		// 				str: chalk.bold(msg.account.acct) + ' favourited your update: ' + this._cleanString(msg.status.content),
		// 				channel: channel,
		// 				timestamp: Date.parse(msg.created_at)
		// 			});
		// 		} else if (msg.type === 'mention') {
		// 			this.emit({
		// 				str: chalk.bold(msg.account.acct) + ' mentioned you ' + this._cleanString(msg.status.content),
		// 				channel: channel,
		// 				timestamp: Date.parse(msg.created_at)
		// 			});
		// 		} else {
		// 			this._vorpal.log('channel', msg);
		// 		}
	}
}

// signUp returns a Promise for a set of options. If the promise resolves, these options
// will be saved into the user preferences, then a new instance of this platform's `Account`
// will be instantiated.
export function signUp() {
	return new Promise((resolve, reject) => {
		/// Twitter only has one known API endpoint, no need to handle this.
		let server = "api.twitter.com";

		let oauth = new OAuth(
			"https://" + server + "/oauth/request_token",
			"https://" + server + "/oauth/access_token",
			soclialConsumerKey,
			soclialConsumerSecret,
			"1.0A",
			"oob",
			"HMAC-SHA1"
		);

		// 		console.log('oauth instance: ', oauth);

		oauth.getOAuthRequestToken(
			(
				error,
				oauthToken,
				oauthTokenSecret,
				oauthAuthorizeUrl,
				additionalParameters
			) => {
				if (error) {
					return reject(
						"Error retrieving the OAuth Request Token: " +
							JSON.stringify(error)
					);
				}

				// 			console.log('Successfully retrieved the OAuth Request Token:', oauthToken,
				// 						', secret: ', oauthTokenSecret,
				// 						', auth url:', oauthAuthorizeUrl);

				let url =
					"http://" +
					server +
					"/oauth/authenticate?oauth_token=" +
					oauthToken;

				// 				this.log(
				// 					"In order to add a new Twitter account, visit ",
				// 					url,
				// 					" to get a verification code."
				// 				);

				return inquirer
					.prompt([
						{
							type: "input",
							name: "code",
							message:
								"In order to add a new Twitter account, visit " +
								url +
								" to get a verification code." +
								"\nEnter verification code: ",
						},
					])
					.then((answer) => {
						if (!answer.code) {
							return reject("No verification code was introduced.");
						}

						// 				console.log('We got verification code:', answer.code);

						oauth.getOAuthAccessToken(
							oauthToken,
							oauthTokenSecret,
							answer.code,
							function(
								error,
								oauthClientToken,
								oauthClientSecret,
								additionalParameters
							) {
								if (error) {
									return reject(
										"Error retrieving the OAuth Client Token: " +
											JSON.stringify(error)
									);
								}

								return resolve({
									clientId: oauthClientToken,
									clientSecret: oauthClientSecret,
									color: "cyan",
									channels: {
										home: {
											color: "cyan",
										},
										mentions: {
											color: "lime",
										},
									},
								});
							}
						);
					});
			}
		);
	});
}

export const defaultBadge = "tw";
export const name = "twitter";
export { TwitterAccount as Account };
