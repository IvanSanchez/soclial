import Account from "../abstracts/account";
import TimeLock from "../timelock";

import inquirer from "inquirer";
// import fetch from "node-fetch";
import fetch from "../retry-fetch";

// 🍂class HackerNewsAccount
// 🍂inherits Account
// A simple platform which displays items from hackernews (news.ycombinator.com).
// By default it re-requests the news feed 30 seconds after the last news items
// were fetched.

class HackerNewsAccount extends Account {
	constructor(master, opts) {
		super(master, opts);

		this._listening = {};
		this._lastStatusesPerChannel = {};
	}

	// 🍂channel story; Feed of newest stories
	listen(channel) {
		if (channel === "story") {
			this._lastStatusesPerChannel["story"] = 0;

			this.verbose("Doing a request for", channel, ".");

			let timelock = new TimeLock();
			this.emit(-Infinity, timelock.promise);
			this._requestChannelOnce(
				channel,
				"https://hacker-news.firebaseio.com/v0/newstories.json",
				timelock
			);
		} else {
			this.error("Unknown channel", channel);
		}

		return this;
	}

	// Returns a Promise that resolves when all the items have been fully resolved
	// (for no reason).
	async _requestChannelOnce(channel, url, timelock) {
		// 		https://hacker-news.firebaseio.com/v0/newstories.json

		let maxId = 0;
		let res;

		try {
			const req = await fetch(url, {logger: this.verbose});
			let ids = await req.json();

			ids = ids.slice(0, 50);

			res = await Promise.all(
				ids.map((id) => {
					return fetch(
						"https://hacker-news.firebaseio.com/v0/item/" +
							Number(id) +
							".json", {logger: this.verbose}
					)
						.then((response) => response.json())
						.then((item) => {
							if (
								!item ||
								item.id <= this._lastStatusesPerChannel[channel]
							) {
								return false;
							}
							maxId = Math.max(maxId, item.id);

							const soclialItem = this._hnStoryToSoclialItem(item);

							this.emit(
								soclialItem.timestamp,
								Promise.resolve(soclialItem)
							);

							return soclialItem;
						});
				})
			);
		} catch (ex) {
			this.error(
				"Failed to fetch hackernews stories:",
				ex,
				", will retry in 30s"
			);
		}

		this._lastStatusesPerChannel[channel] = Math.max(
			maxId,
			this._lastStatusesPerChannel[channel]
		);
		timelock.unlock();
		const nextTimelock = new TimeLock();
		this.emit(Date.now(), nextTimelock.promise);
		setTimeout(
			() => this._requestChannelOnce(channel, url, nextTimelock),
			30000
		);

		return res;
	}

	_hnStoryToSoclialItem(story) {
		return {
			// 			id: this._id,
			accountNumber: this._accountNumber,
			accoundId: "hackernews",
			platform: "hackernews",
			raw: story,
			sender: story.by,
			str: story.title + (story.url ? " " + story.url : ""),
			url: "https://news.ycombinator.com/item?id=" + Number(story.id),
			timestamp: story.time * 1000,
			channel: "story",
		};
	}

	// 	_emitTickItem(channel) {
	// 		this.emit(Date.now(), Promise.resolve({ str: "Tick.", channel: channel, accountNumber: this._accountNumber }));
	// 	}

	unlisten(channel) {
		delete this._listening[channel];

		return this;
	}

	listChannels() {
		return ["story"];
	}
}

// signUp returns a Promise for a set of options. If the promise resolves, these options
// will be saved into the user preferences, then a new instance of this platform's `Account`
// will be instantiated.
export function signUp() {
	console.log('Performing sign-up procedure for a new "hackernews" account');

	// There is nothing asynchronous here, so...

	/// TODO: Extend this with a prompt for the default channel to listen to, or a prompt for
	/// the badge.

	return inquirer
		.prompt([
			{
				type: "list",
				name: "confirm",
				message:
					'You are going to add a "hackernews" platform. Is this right?',
				choices: ["yes", "no"],
			},
		])
		.then((answer) => {
			if (answer.confirm === "yes") {
				return {
					// The hackernews platform does not need any parameters to initialize
					// an account. In a normal case, this response would include
					// auth tokens and so on.
					channels: {
						story: {
							/*, color: "orange"*/
						},
					},

					color: "orange",

					badge: "hn",
				};
			} else {
				throw new Error("Not creating the new hackernews account then.");
			}
		});
}

export const defaultBadge = "hn"; // or '⏰'
export const name = "hackernews";
export { HackerNewsAccount as Account };
