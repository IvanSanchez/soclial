/*
 * This is a template platform module.
 *
 * Every platform is a ES2015 module which must expose:
 * - Account (a subclass of 'Account'; a bit confusing, I know)
 * - name (a String)
 * - signUp (a function that must return a Promise to a set of config options
 *      for a new configured account)
 */

import Account from "../abstracts/account";

class TemplateAccount extends Account {
	// Constructor receives:
	// - A dictionary with references to callbacks provided by soclial
	// - The account's options
	constructor(master, opts) {
		/*
		const {
			// emit(timestamp, promiseToSoclialItem) sends an item promise to the queue
			emit,

			// The account's numeric ID, which must be carried to the emitted soclial items
			accId,

			// The account's badge as provided by the config
			badge,

			// The account's prompt as provided by soclial
			prompt,

			// Logging facilities
			log,
			error,
			verbose,

		} = master;

		 */
		// ...
	}
	// ...
}

export { DemoAccount as Account };

export function signUp() {
	// Prompt for stuff, returning a Promise for the config opts for a new instance
	return Promise.resolve({});
}

export const name = "template";
