/*
 * Unsplash account.
 *
 * Kinda counterproductive, since unsplash is all about photos and
 * soclial is all about commandline. But it's a good way to have more
 * items with media.
 */

import Account from "../abstracts/account";

import TimeLock from "../timelock";

import inquirer from "inquirer";
import fetch from "../retry-fetch";

// // Weird, unsplash-js doesn't import "fetch" :-/
// global.fetch = fetch;
// import Unsplash from 'unsplash-js';

class UnsplashAccount extends Account {
	constructor(master, opts) {
		super(master, opts);
		this._lastTimePerChannel = {};
	}

	// 🍂channel photos; Feed of newest photos
	listen(channel) {
		if (channel === "photos") {
			this._lastTimePerChannel["photos"] = 0;

			this.verbose("Doing a request for", channel, ".");

			let timelock = new TimeLock();
			this.emit(-Infinity, timelock.promise);
			this._requestChannelOnce(channel, timelock);
		} else {
			this.error("Unknown channel", channel);
		}

		return this;
	}

	async _requestChannelOnce(channel, timelock) {
		// 		const req = await this._api.photos.listPhotos(1, 50, "latest");

		let url = "https://api.unsplash.com/photos?per_page=50";
		if (channel === "photos") {
			url += "&order_by=latest";
		} else if (channel === "popular") {
			url += "&order_by=popular";
		} else {
			this.error("Could not guess URL for channel", channel);
			return;
		}

		try {
			const res = await fetch(url, {
				method: "GET",
				headers: {
					"Accept-Version": "v1",
					Authorization:
						"Client-ID f2ec680adb8424ff52b670f2bed10458bf1c4c5401be57764f9be7f0a5b5d93d",
				},
				logger: this.verbose
			});

			// Rate limits in unsplash are reset each hour, but we don't know
			// when exactly. At every hour on the clock? Starts counting down
			// after the first request? Who knows!
			const rateLimitTotal = res.headers.get("x-ratelimit-limit");
			const rateLimitRemaining = res.headers.get("x-ratelimit-remaining");

			// Calculate the theoretical optimum wait, then grow it based on
			// how much of the rate limit has been spent. In any case, no less than
			// 20secs.
			let wait = Math.max(
				(3600000 / rateLimitTotal) * (rateLimitTotal / rateLimitRemaining),
				20000
			);

			const list = await res.json();
			let maxTime = 0;

			for (let photo of list) {
				const updatedAt = Date.parse(photo.updated_at);
				maxTime = Math.max(maxTime, updatedAt);
				if (updatedAt > this._lastTimePerChannel[channel]) {
					this.emit(
						Date.parse(photo.updated_at),
						normalizeUnsplashPhoto(photo, channel)
					);
				}
			}

			this._lastTimePerChannel[channel] = Math.max(
				this._lastTimePerChannel[channel],
				maxTime
			);

			timelock.unlock();

			this.verbose(
				"Waiting for ",
				wait,
				"msec. (",
				rateLimitRemaining,
				"/",
				rateLimitTotal,
				")"
			);
			const nextTimelock = new TimeLock();
			this.emit(Date.now(), nextTimelock.promise);
			setTimeout(() => this._requestChannelOnce(channel, nextTimelock), wait);
		} catch(ex) {
			this.error(
				"Failed to fetch unsplash photos:",
				ex,
				", will retry in 30s"
			);
			const nextTimelock = new TimeLock();
			this.emit(Date.now(), nextTimelock.promise);
			setTimeout(() => this._requestChannelOnce(channel, nextTimelock), 30000);
		}
	}
}

// Takes the JSON structure of a Unsplash photo, and returns a soclial item.
function normalizeUnsplashPhoto(photo, channel) {
	const item = {
		raw: photo,
		str: photo.description ? photo.description : "",
		sender: photo.user.username,
		url: photo.links.html,
		media: [photo.urls.regular],
		timestamp: Date.parse(photo.updated_at),
		channel: channel,
		// echoed: {},
		// quoted: {},
	};

	return Promise.resolve(item);
}

export { UnsplashAccount as Account };

// signUp returns a Promise for a set of options. If the promise resolves, these options
// will be saved into the user preferences, then a new instance of this platform's `Account`
// will be instantiated.
export async function signUp() {
	console.log('Performing sign-up procedure for a new "unsplash" account');

	// There is nothing asynchronous here, so...

	/// TODO: Extend this with a prompt for the default channel to listen to, or a prompt for
	/// the badge.

	const answer = await inquirer.prompt([
		{
			type: "list",
			name: "confirm",
			message:
				'You are going to add a "unsplash" platform. Is this right? Currently a soclial "unsplash" platform supports only public photos.',
			choices: ["yes", "no"],
		},
	]);

	if (answer.confirm === "yes") {
		return {
			// The unsplash platform does not need any parameters to initialize
			// an account (as logged-in users are not implemented).
			channels: { photos: { color: "white" } },

			color: "white",
		};
	} else {
		throw new Error("Not creating the new unsplash account then.");
	}
}

export const defaultBadge = "un"; // or '⏰'
export const name = "unsplash";
