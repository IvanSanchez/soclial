import chalkPipe from "chalk-pipe";

/// TODO: make these replacements configurable. Somehow "␤" and "␍" UTF8 chars
/// break spacing on terminal emulators.
let newline = " " + String.fromCharCode(9252) + " "; // "␤", http://www.fileformat.info/info/unicode/char/2424/index.htm
let carReturn = " " + String.fromCharCode(9229) + " "; // "␍", http://www.fileformat.info/info/unicode/char/240d/index.htm

// Turns a soCLIal item into a string, prettyfied with chalk.
export default function stringify(conf, item) {
	let { str, channel, realtime, timestamp, account } = item;

	let sender = item.sender;
	let quotedStr = "";
	let echoedStr = "";

	const usernameColour = conf.get("global.colours.usernames");
	const updateColour = conf.get("global.colours.updates");
	const mediaColour = conf.get("global.colours.media");

	let formattedMedia = "";

	if (item.media && item.media.length) {
		formattedMedia = chalkPipe(mediaColour)("  Media: " + item.media.join(" "));
		// 		for(let i=0, l=item.media.length; i<l; i++) {
		// 		}
	}

	// This is an echo (RT/boost/share) of some other status
	if (item.echoed) {
		echoedStr = "R" + stringify(conf, item.echoed);
		formattedMedia = "";
	}

	// This is a quote (RT with comments) of some other status
	if (item.quoted) {
		quotedStr = "\n                           " + stringify(conf, item.quoted);
	}

	str = str.replace(/\n/g, newline);
	str = str.replace(/\r/g, carReturn);

	let formatted =
		(sender ? " <" + chalkPipe("bold")(sender) + ">" : "") +
		" " +
		chalkPipe(updateColour)(str) +
		echoedStr +
		formattedMedia +
		quotedStr;

	return formatted;
}
