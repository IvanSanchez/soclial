// This is a filter. A filter takes in an item, and returns either:
// - an item
// - a promise for an item
// - a falsy value
// - a promise for a falsy value
// Items can be modified, kept the same, or filtered out altogether.

// Filters will be chained together by the soclial core, passed as parameters
// to itemPromise.then(...). This allows for both sync and async filters.

import urlRegexFn from "url-regex";
const urlRegex = urlRegexFn(); // For some reason the module exports a function

// import { extract } from 'article-parser';
import Deshortifier from "deshortify";
// import Deshortifier from '../../../deshortify/src/deshortify'
import TimeLock from "../timelock";

// console.log(Deshortifier);

let deshortifier = new Deshortifier({ verbose: false });

// // Number of pending requests; will impact the timeout, adding one second per pending request.
// let pending = 0;
// let cache = {};

let pending = 0;
const queue = [];

for (let i = 0; i < 10; i++) {
	queue.push(new TimeLock().unlock());
}

export default function deshortify(master, item) {
	// This filter takes the text in the item, finds the URLs, and deshortifies them.

	let urls = item.str.match(urlRegex);

	if (!urls) {
		urls = [];
	} // Item itself doesn't have any URLs, but might have an echo/quote

	// Filter out ellipses. They mess up stuff in twitter.
	urls = urls.map(function(url) {
		return url.split("…")[0];
	});

	item._deshortifyOriginalUrls = urls;
	// 	console.log('Detected URLs:', urls);

	const ownUrlPromises = Array.from(urls).map((url) => {
		const myAddedLock = new TimeLock();
		queue.push(myAddedLock);
		pending++;

		// 		master.verbose(
		// 			"deshortify filter processing queue length: ",
		// 			pending /*, queue.map(i=>i.locked)*/
		// 		);

		return queue.shift().promise.then(() => {
			// master.verbose('deshortifying. queue length: ', pending, queue.map(i=>i.locked));
			// master.verbose("deshortifying. queue length: ", pending);

			const deshortifyPromise = deshortifier
				.deshortify(url)
				.then((deshortified) => {
					return [url, deshortified];
				})
				.catch((err) => {
					master.error(err, url);
					return [url, url];
				});

			let timeout;

			return Promise.race([
				deshortifyPromise,
				new Promise((resolve, reject) => {
					// master.verbose('Setting timeout for url: ', url);

					timeout = setTimeout(() => {
						master.error("Resolving URLs took too long. URL was:", url);
						resolve([url, url]);
					}, 30000);
				}),
			]).then((v) => {
				clearTimeout(timeout);
				// master.verbose("Done.", v);
				myAddedLock.unlock();
				pending--;
				return v;
			});
		});
	});

	let ownItemPromise = Promise.all(ownUrlPromises).then((resolvedUrls) => {
		item._deshortifyResolvedUrls = resolvedUrls;

		// 		master.verbose('resolved URLs', resolvedUrls);

		resolvedUrls.forEach(([original, replacement]) => {
			if (original !== replacement) {
				master.verbose("Replacing", original, "→", replacement);
				// 				console.log('Replacing: ', original, ' → ', replacement);
				item.str = item.str.replace(original, replacement);
			}
		});

		return item;
	});

	// Does this item echo another? Then deshortify its URLs too.
	if (item.echoed) {
		//             console.log('Deshortifying also: ', item.echoed);
		let echoedItemPromise = deshortify(master, item.echoed);

		ownItemPromise = Promise.all([ownItemPromise, echoedItemPromise]).then(
			([own, echoed]) => {
				own.echoed = echoed;
				return own;
			}
		);
	}

	// Does this item quote another? Then deshortify its URLs too.
	if (item.quoted) {
		// 		console.log('Deshortifying also: ', item.quoted);
		let echoedItemPromise = deshortify(master, item.quoted);

		ownItemPromise = Promise.all([ownItemPromise, echoedItemPromise]).then(
			([own, quoted]) => {
				own.quoted = quoted;
				return own;
			}
		);
	}

	return ownItemPromise;
}
