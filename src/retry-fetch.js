// A simple wrapper over node-fetch, which does a few retries spaced by
// 4-8-16-32 seconds each.

// It also prettifyes 5xx errors (which are not rejected promises, but
// a specific case of resolved response)

import fetch from "node-fetch";

export default function retryFetch(url, options = {}, retries = 5) {
	var res = fetch(url, options).then((response) => {
		if (!response.ok) {
			return Promise.reject(response.statusText);
		} else {
			return response;
		}
	});

	if (retries > 0) {
		return res.catch((err) => {
			(options.logger || console.log)(
				"Will retry",
				url,
				retries,
				"after",
				64000 >> retries,
				"msec"
			);
			return new Promise((res, rej) => {
				setTimeout(() => {
					(options.logger || console.log)("Retrying", url, retries);
					retryFetch(url, options, retries - 1)
						.then(res)
						.catch(rej);
				}, 64000 >> retries);
			});
		});
	} else {
		return res;
	}
}
