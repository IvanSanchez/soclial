// 🍂class Account
// A base abstract class for all social platforms. It's possible to use more than
// an instance of the same  `Platform`, e.g. more than one account in a social
// network.

export default class Account {
	// 🍂constructor constructor(callback: Function, opts: Object)
	// Any platform will be initialized with a `callback` function, that will
	// receive the emitted status updates.
	constructor(master, opts) {
		if (this._opts === undefined) {
			this._opts = {};
		}
		this._master = master;

		const { emit, log, error, verbose, prompt, accId, badge } = master;

		this.log = log;
		this.error = error;
		this.verbose = verbose;
	}

	get opts() {
		return this._opts;
	}

	set opts(newOpts) {
		return (this._opts = Object.assign(this._opts, newOpts));
	}

	// 🍂method listen(callback: Function, channel: String): this
	// Starts listening for updates at the given "channel". A "channel" is a
	// string with semantics depending on the platform. A platform might have
	// channels like "wall", "stream", "replies", "dms", lists, and so on.
	listen(channel) {
		return this;
	}

	// 🍂method listen(channel: String): this
	// Stops listening for updates at the given channel. Corresponds to a previous
	// `listen()` call.
	unlisten(channel) {
		return this;
	}

	// 🍂method listChannels(): Array
	// Returns a list of *well-known* channels for this account. This doesn't
	// mean that these channels are the only ones available.
	listChannels() {
		return [];
	}

	// 🍂method post(str)
	// Given a string, posts it in the platform (as an tweet/toot/update/whatever)
	post(str) {}

	// 🍂method reply(item, str)
	// Posts the given string as a reply to the given item. Item shall be from
	// the same platform and account.
	reply(item, str) {}

	// 🍂method echoify(item): String
	// Returns a string representing the text content of the item, suitable
	// for echoing that item in other platforms. Item shall be from the same
	// platform as the account.
	// e.g. return "RT @username: foobar" for tweets, so it can be boosted in mastodon.
	echoify(item) {}

	// 🍂method echo(item, str): String
	// Echoes (RTs/boosts/reshares) an item. If the item cannot be directly used
	// due to platform incompatibility, the given string will be posted instead,
	// most probably using also the item's URL (the string is assumed to be the
	// echoify()ed version of the item
	echo(item, str) {}

	// 🍂section Internal methods
	// 🍂method emit(timestamp: Number, data: Promise): this
	// Makes this `Platform` emit a status update to be printed on the CLI (after
	// filtering/transforming by plugins). Needs some normalized data about the
	// update, but can include optional metadata as given from the platform.
	// This is meant to be called only from subclasses.
	emit(timestamp, data) {
		this._master.emit(
			timestamp,
			data.then((d) => {
				if (d) {
					d.accountNumber = this._master.accId;
				}
				return d;
			})
		);
		return this;
	}

	// 🍂method log(...)
	// Logs the given argument(s) to the console. Use for informative messages.

	// 🍂method error(...)
	// Logs the given argument(s) to the console. Use for error messages.

	// 🍂method verbose(...)
	// Logs the given argument(s) to the console. Use for verbose, non-critical information.
}
