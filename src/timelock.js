// A simple wrapper over a Promise with a timestamp, to save a bit of code in the
// platforms code.

// The idea is to provide a way to resolve a promise asynchronously, without
// adding one more level of indentation and preventing a bit of callback hell.

export default class TimeLock {
	constructor() {
		this._promise = new Promise((resolve) => {
			this._onUnlock = resolve;
		});
		this.locked = true;
	}

	get promise() {
		return this._promise;
	}

	unlock() {
		this._onUnlock(false);
		this.locked = false;
		return this;
	}
}
