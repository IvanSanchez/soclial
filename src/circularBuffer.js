// import stringify from 'json-stringify-safe';

import fs from "fs";

let lastId = 0;

if (fs.accessSync("debug/")) {
	fs.mkdir("debug/");
}

const maxId = 25 * 25 * 10;
const dictLetters = "abcdefghijkmnopqrstuvwxyz";
const dictNumbers = "0123456789";

let buffer = {};

function idToString(id) {
	return (
		dictLetters[Math.floor(id / 250) % 25] +
		dictLetters[Math.floor(id / 10) % 25] +
		dictNumbers[id % 10]
	);
}

// function stringToId(str) {
//
// }

export function get(str) {
	return buffer[str];
}

export function push(data) {
	let str = idToString(lastId++ % maxId);
	buffer[str] = data;

	// 	fs.writeFile('debug/' + str + '.json', JSON.stringify(data, ' '));
	// 	console.log(data);
	fs.writeFile(
		"debug/" + str + ".json",
		JSON.stringify(data, null, " "),
		(err) => {}
	);

	return str;
}
